package com.exersice;

import com.exersice.utils.InputScanner;

public class Login {
    public static void main(String[] args) {
        String username = "username";
        String password = "password";

        InputScanner scanner = new InputScanner(System.in);
        System.out.print("Please Enter your username: ");
        String inputUsername = scanner.next();
        System.out.print("Please Enter your password: ");
        String inputPassword = scanner.next();

        boolean isValid = checkEmpty(inputUsername, inputPassword);

        if (!isValid) {
            System.out.println("Username or password must not be empty");
            return;
        }
        if (!username.equals(inputUsername)) {
            System.out.println(String.format("User with username %s not found", username));
            return;
        }
        if (!password.equals(inputPassword)) {
            System.out.println("The password you entered is incorrect");
            return;
        }
        System.out.println("Login success");
    }

    private static boolean checkEmpty(String username, String password) {
        return !username.trim().isEmpty() && !password.trim().isEmpty();
    }
}
