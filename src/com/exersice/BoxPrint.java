package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.print;

public class BoxPrint {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        String text = scanner.next();
        String line = new String(new char[text.length() + 4]).replace('\0','-');
        print(line);
        print(String.format("| %s |",text));
        print(line);
    }
}
