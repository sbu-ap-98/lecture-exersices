package com.exersice;

import com.exersice.utils.InputScanner;

public class DotPlot {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        String text = scanner.next();

        System.out.print(" ");
        for (char ch : text.toCharArray()){
            System.out.print(" ");
            System.out.print(ch);
        }
        System.out.println();

        for (char ch : text.toCharArray()){
            System.out.print(ch);
            System.out.print(" ");
            for (char ch2 : text.toCharArray()){
                if (ch == ch2){
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
