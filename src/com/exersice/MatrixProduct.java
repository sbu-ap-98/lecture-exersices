package com.exersice;

import java.util.Arrays;

import static com.exersice.utils.Printer.print;

public class MatrixProduct {
    public static void main(String[] args) {

        int[][] first = {
                {1, 2, 3, 4},
                {4, 3, 2, 1},
                {1, 1, 1, 1},
        };

        int[][] second = {
                {1, 1, 1},
                {5, 2, 1},
                {3, 4, 8},
                {1, 6, 0},
        };

        int[][] product = product(first, second);

        for (int[] row : product) {
            print(Arrays.toString(row));
        }

    }

    public static int[][] product(int[][] first, int[][] second) {
        int[][] product = new int[first.length][second[0].length];
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second[0].length; j++) {
                product[i][j] = productRowColumn(first, second, i, j);
            }
        }
        return product;
    }

    public static int productRowColumn(int[][] rowMatrix, int[][] columnMatrix, int row, int column) {
        int ret = 0;
        for (int i = 0; i < rowMatrix[0].length; i++) {
            ret += rowMatrix[row][i] * columnMatrix[i][column];
        }
        return ret;
    }
}
