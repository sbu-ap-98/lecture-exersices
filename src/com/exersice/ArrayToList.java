package com.exersice;

import java.util.*;
import java.util.stream.Collectors;

import static com.exersice.utils.Printer.print;

public class ArrayToList {
    public static void main(String[] args){
        int[][] array = {
                {1,2,3,4,5,6},
                {2,3,4,5,6,7},
                {3,4,5,6,7,8},
                {4,5,6,7,8,9}
        };

        List<List<Integer>> converted = convert(array);

        print(converted);
    }

    private static List<List<Integer>> convert(int[][] array) {
        List<List<Integer>> convert = new ArrayList<>();
        for (int[] row : array){
            List<Integer> list = Arrays.stream(row).boxed().collect(Collectors.toList());
            convert.add(list);
        }
        return convert;
    }
}
