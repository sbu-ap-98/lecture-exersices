package com.exersice;

import com.exersice.utils.InputScanner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.exersice.utils.Printer.print;

public class ExtractWords {
    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);
        String text = scanner.next();

        List<String> words = extractWords(text);

        print(words);
    }

    private static List<String> extractWords(String text) {
        List<String> output = new ArrayList<>();
        String[] lines = text.split("\\n");
        for (String line : lines) {
            line = line.replace(",", "").replace(".", "");
            String[] words = line.split(" ");
            output.addAll(Arrays.asList(words));
        }

        return output;
    }
}
