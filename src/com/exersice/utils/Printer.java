package com.exersice.utils;

public class Printer {
    public static void print(int i){
        System.out.println(i);
    }
    public static void print(String str){
        System.out.println(str);
    }
    public static void print(double d){
        System.out.println(d);
    }
    public static void print(float f){
        System.out.println(f);
    }
    public static void print(Object object){
        System.out.println(object.toString());
    }
    public static void print(boolean bool){
        System.out.println(bool);
    }
}
