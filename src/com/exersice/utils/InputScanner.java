package com.exersice.utils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Scanner;

public class InputScanner implements Iterator<String>, Closeable {
    private final Scanner scanner;

    public InputScanner(InputStream inputStream) {
        scanner = new Scanner(inputStream).useDelimiter("\n");
        System.out.println("Use Enter to separate your inputs.");
    }

    public int nextInt() {
        return scanner.nextInt();
    }

    /**
     * fix console input that does not send new line to input stream when user submits an input.
     *
     * @return user input until Enter key press.
     */
    public String nextLine() {
        return scanner.next();
    }

    public Double nextDouble() {
        return scanner.nextDouble();
    }

    public BigDecimal nextDecimal() {
        return scanner.nextBigDecimal();
    }

    /**
     * casting next byte of the stream into char
     *
     * @return next character in input stream
     */
    public char nextChar() {
        return (char) scanner.nextByte();
    }

    public byte nextByte() {
        return scanner.nextByte();
    }

    @Override
    public void close() throws IOException {
        scanner.close();
    }

    @Override
    public boolean hasNext() {
        return scanner.hasNext();
    }

    @Override
    public String next() {
        return scanner.next();
    }

    public float nextFloat() {
        return scanner.nextFloat();
    }
}
