package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.print;

public class Plaindrome {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        String content = scanner.next();
        print(new StringBuilder(content).reverse().toString().equals(content));
    }
}
