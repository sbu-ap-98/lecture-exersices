package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.print;

public class Fibo {
    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);

        int index = scanner.nextInt();

        print(fibo(index));
    }

    public static int fibo(int index) {
        if (index == 0 || index == 1) return 1;
        int prev = 1;
        int current = 1;
        for (int i = 2; i <= index; i++) {
            int temp = current;
            current += prev;
            prev = temp;
        }
        return current;
    }
}
