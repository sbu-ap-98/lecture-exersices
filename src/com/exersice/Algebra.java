package com.exersice;

import java.util.Scanner;

import static com.exersice.utils.Printer.*;

public class Algebra {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useDelimiter("");
        char p = scanner.next().charAt(0);

        switch (scanner.next().charAt(0)) {
            case 'a':
                and(p, scanner.next().charAt(0));
                break;
            case 'o':
                or(p, scanner.next().charAt(0));
                break;
            case 'x':
                xor(p, scanner.next().charAt(0));
                break;
            case 't':
                then(p, scanner.next().charAt(0));
                break;
        }
    }

    public static void and(char first, char second) {
        print(first == '1' && second == '1');
    }

    public static void or(char first, char second) {
        print(first == '1' || second == '1');
    }

    public static void xor(char first, char second) {
        print(first == second);
    }

    public static void then(char first, char second) {
        print(first == '0' || second == '1');
    }
}
