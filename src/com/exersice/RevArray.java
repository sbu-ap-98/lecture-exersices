package com.exersice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.exersice.utils.Printer.print;

public class RevArray {
    public static void main(String[] args){

        int[] items = {0,1,2,3,4,5};
        String[] names = {"ali","reza","shayan"};

        print(Arrays.toString(reverse(items)));
        print(Arrays.toString(reverse(names)));

    }

    public static int[] reverse(int[] items){
        int[] reverse = new int[items.length];
        for (int i=0;i<items.length;i++){
            reverse[items.length-i-1] = items[i];
        }
        return reverse;
    }

    public static <T> T[] reverse(T[] items){
        List<T> reverse = new ArrayList<>();
        for (int i=0;i<items.length;i++){
            reverse.add(items[items.length -i -1]);
        }
        return reverse.toArray(items);
    }
}
