package com.exersice;

import com.exersice.utils.InputScanner;

import java.util.Random;

import static com.exersice.utils.Printer.print;

public class Candy {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);

        String[] names =  readNamesFromFile(scanner);

        int index = new Random().nextInt(names.length);

        print(String.format("Winner of candy is: %s",names[index]));
    }

    private static String[] readNamesFromFile(InputScanner scanner) {
        int length = scanner.nextInt();
        String[] names = new String[length];
        for (int i=0;i<length;i++){
            names[i] = scanner.next();
        }
        return names;
    }
}
