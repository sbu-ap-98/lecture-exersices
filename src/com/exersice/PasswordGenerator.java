package com.exersice;

import com.exersice.utils.InputScanner;

import java.security.SecureRandom;

import static com.exersice.utils.Printer.print;

public class PasswordGenerator {
    private int lowercaseStart = 97;
    private int uppercaseStart = 65;
    private int charactersLength = 26;

    private int startIndex = 33;
    private int endIndex = 127;

    private SecureRandom random = new SecureRandom();

    int[] specialCharacters = new int[endIndex - startIndex - (2 * charactersLength)];

    public PasswordGenerator() {
        initSpecialCharacters();
        String s;
    }

    private void initSpecialCharacters() {
        int index = 0;
        for (int i = startIndex; i < endIndex; i++) {
            if (!isLowercaseCharacter(i) && !isHigherCaseCharacter(i)) {
                specialCharacters[index] = i;
                index++;
            }
        }
    }

    private boolean isHigherCaseCharacter(int i) {
        return i >= uppercaseStart && i < uppercaseStart + charactersLength;
    }

    private boolean isLowercaseCharacter(int i) {
        return i >= lowercaseStart && i < lowercaseStart + charactersLength;
    }

    private char generateRandomLowercase(){
        int ch = random.nextInt(charactersLength) + lowercaseStart;
        return (char) ch;
    }

    private char generateRandomUppercase(){
        int ch = random.nextInt(charactersLength) + uppercaseStart;
        return (char) ch;
    }

    private char generateRandomSpecialCharacter(){
        int index = random.nextInt(specialCharacters.length);
        return (char) specialCharacters[index];
    }

    public String generate(int length) {
        if (length < 8) {
            throw new IllegalArgumentException("Password length must be at least 8");
        }

        StringBuilder passwordBuilder = new StringBuilder();

        passwordBuilder.append(generateRandomLowercase());
        passwordBuilder.append(generateRandomUppercase());
        passwordBuilder.append(generateRandomSpecialCharacter());

        for (int i=0;i<length-3;i++){
            int state = random.nextInt(3);
            if (state==0){
                passwordBuilder.append(generateRandomLowercase());
            } else if (state==1){
                passwordBuilder.append(generateRandomUppercase());
            } else {
                passwordBuilder.append(generateRandomSpecialCharacter());
            }
        }

        return shuffle(passwordBuilder.toString());
    }

    private String shuffle(String text){
        char[] characters = text.toCharArray();
        for (int i=0;i<characters.length;i++){
            int j = random.nextInt(characters.length);
            char temp = characters[i];
            characters[i] = characters[j];
            characters[j] = temp;
        }
        return new String(characters);
    }


    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        int length = scanner.nextInt();
        PasswordGenerator generator = new PasswordGenerator();
        String password = generator.generate(length);
        print(password);
    }
}
