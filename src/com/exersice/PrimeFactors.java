package com.exersice;

import com.exersice.utils.InputScanner;

import java.util.ArrayList;
import java.util.List;

import static com.exersice.utils.Printer.print;

public class PrimeFactors {
    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);
        int number = scanner.nextInt();
        List<Integer> primeFactors = getPrimeFactorsOf(number);
        print(primeFactors);
    }

    private static List<Integer> getPrimeFactorsOf(int number) {
        List<Integer> primes = getPrimeNumbersTill(number);
        List<Integer> primeFactors = new ArrayList<>();
        for (Integer prime : primes) {
            if (number % prime == 0) {
                primeFactors.add(prime);
            }
        }
        return primeFactors;
    }

    private static List<Integer> getPrimeNumbersTill(int number) {
        List<Integer> primes = new ArrayList<>();
        if (number < 2) {
            return primes;
        }
        primes.add(2);
        if (number < 3) {
            return primes;
        }
        for (int i = 3; i < number / 2; i++) {
            boolean isPrime = true;
            for (Integer prime : primes) {
                isPrime = isPrime && i % prime != 0;
            }
            if (isPrime) {
                primes.add(i);
            }
        }
        boolean isPrime = true;
        for (Integer prime : primes) {
            isPrime = isPrime && number % prime != 0;
        }
        if (isPrime) {
            primes.add(number);
        }
        return primes;
    }
}
