package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.print;

public class EvenSum {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        int length = scanner.nextInt();
        int[] numbers = new int[length];
        for (int i=0;i<length;i++){
            numbers[i] = scanner.nextInt();
        }

        int evenSum = EventSum(numbers);
        print(evenSum);
    }

    private static int EventSum(int[] numbers) {
        int sum = 0;
        for (int i=0;i<numbers.length;i+=2){
            sum += numbers[i];
        }
        return sum;
    }
}
