package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.*;

public class Watermelon {
    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);

        int weight = scanner.nextInt();

        if (canDivide(weight)) {
            print(String.format("%d can be divided into to even weights", weight));
        } else {
            print(String.format("Sorry, cannot divide %d into to even weights", weight));
        }
    }

    public static boolean canDivide(int number) {
        return number % 2 == 0;
    }


}
