package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.print;

public class Distinct {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        String text = scanner.next();
        StringBuilder result = new StringBuilder();

        char lastChar = '\0';
        for (int i=0;i<text.length();i++){
            char currentChar = text.charAt(i);
            if (currentChar != lastChar){
                result.append(currentChar);
            }
            lastChar = currentChar;
        }

        print(result.toString());
    }
}
