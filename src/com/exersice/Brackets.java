package com.exersice;

import com.exersice.utils.InputScanner;

public class Brackets {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        int num = scanner.nextInt();

        printBrackets(num);
    }

    private static void printBrackets(int num) {
        System.out.print('[');
        if (num>0){
            printBrackets(num-1);
        }
        System.out.print(']');
    }
}
