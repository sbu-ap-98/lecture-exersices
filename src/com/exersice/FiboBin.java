package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.print;

public class FiboBin {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        int number = scanner.nextInt();

        int fibo = Fibo.fibo(number);
        int bin = bin(number);
        print(fibo + bin);
    }

    private static int bin(int number) {
        return Integer.toBinaryString(number).replace("0","").length();
    }
}
