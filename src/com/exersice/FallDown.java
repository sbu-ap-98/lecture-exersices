package com.exersice;

import static com.exersice.utils.Printer.print;

public class FallDown {
    public static void main(String[] args) {
        String[] map = {
                "# ########",
                "#        #",
                "##### ####",
                "#        #",
                "## #######",
                "#        #",
                "###### ###",
                "#        #",
                "###### ###"
        };

        String[] fallDownMap = generateFallDown(map);
        for (String line : fallDownMap) {
            print(line);
        }
    }

    private static String[] generateFallDown(String[] map) {
        String[] falldown = new String[map.length];
        int lastIndex = map[0].indexOf(' ');
        falldown[0] = map[0].replace(' ', 'o');
        for (int i = 2; i < map.length; i += 2) {
            int newIndex = map[i].indexOf(' ');

            int minIndex = Math.min(lastIndex, newIndex);
            int maxIndex = Math.max(lastIndex, newIndex);

            falldown[i - 1] = map[i - 1].substring(0, minIndex) + map[i - 1].substring(minIndex, maxIndex + 1).replace(' ', 'o') + map[i - 1].substring(maxIndex + 1);
            falldown[i] = map[i].replace(' ', 'o');

            lastIndex = newIndex;
        }
        return falldown;
    }
}
