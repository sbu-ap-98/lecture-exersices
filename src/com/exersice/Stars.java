package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.*;

public class Stars {
    public static void main(String[] args){
        InputScanner scanner = new InputScanner(System.in);
        int numbers = scanner.nextInt();

        for (int i=1;i<=numbers;i++){
            printStars(i);
        }

        printStarsRecursive(1,numbers);


    }

    public static void printStars(int numbers){
        print(new String(new char[numbers]).replace('\0','*'));

    }

    public static void printStarsRecursive(int current,int max){
        printStars(current);
        if (current<max) printStarsRecursive(++current,max);
    }
}
