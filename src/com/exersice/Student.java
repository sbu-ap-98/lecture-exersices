package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.*;

public class Student implements Comparable<Student> {

    private String name;
    private float gpa;

    @Override
    public int compareTo(Student student) {
        int gpaCompare = Float.compare(this.gpa, student.gpa);
        if (gpaCompare != 0) return gpaCompare;
        return this.name.compareTo(student.name);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", gpa=" + gpa +
                '}';
    }

    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);

        Student first = new Student();
        first.name = scanner.nextLine();
        first.gpa = scanner.nextFloat();

        Student second = new Student();
        second.name = scanner.nextLine();
        second.gpa = scanner.nextFloat();

        int compare = first.compareTo(second);
        if (compare > 0) {
            print(first);
            print(second);
        } else if (compare < 0) {
            print(second);
            print(first);
        } else {
            print("students are equal");
            print(first);
            print(second);
        }
    }


}
