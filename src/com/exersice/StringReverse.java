package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.print;

public class StringReverse {
    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);
        String content = scanner.nextLine();
        print(new StringBuilder(content).reverse());
    }
}
