package com.exersice;

import com.exersice.utils.InputScanner;

import static com.exersice.utils.Printer.print;

public class Range {
    double start;
    double end;

    public boolean isInRange(double number) {
        return start <= number && end >= number;
    }

    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);
        Range range = new Range();

        range.start = scanner.nextDouble();
        range.end = scanner.nextDouble();

        while (!range.isInRange(scanner.nextDouble())) {
        }

        print("your number is in range");
    }
}
