package com.exersice;

import com.exersice.utils.InputScanner;

import java.util.Random;

import static com.exersice.utils.Printer.print;

public class Guess {
    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);
        int target = new Random().nextInt(100);
        int guess;
        while ((guess = scanner.nextInt()) != target) {
            if (guess > target) {
                print("your guess is higher that target number");
            } else {
                print("your guess is lower that target number");
            }
        }

        print("your guess is right");
    }
}
