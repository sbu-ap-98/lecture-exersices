package com.exersice;

import com.exersice.utils.InputScanner;

import java.math.BigInteger;

import static com.exersice.utils.Printer.print;

public class Factor {
    public static void main(String[] args) {
        InputScanner scanner = new InputScanner(System.in);
        int number = scanner.nextInt();
        long start = System.nanoTime();
        System.out.println(start);
        System.out.println(System.currentTimeMillis());
        factorial(number);
        long end = System.nanoTime();
        System.out.println(end - start);
        print(factorial(number));
    }

    public static BigInteger factorial(int number) {
        if (number == 0) return BigInteger.ZERO;
        if (number == 1) return BigInteger.ONE;
        if (number == 2) return BigInteger.TWO;
        return factorial(number - 1).multiply(BigInteger.valueOf(number));
    }
}
